"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_predaprey.py
```
"""

import os
import sys

import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.predaprey.solver import Simul

here = os.path.abspath(os.path.dirname(__file__))
path_dir_save = os.path.abspath(here + '/../fig/')
if not os.path.exists(path_dir_save):
    os.mkdir(path_dir_save)

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.1
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs + 2.0)
sim.state.state_phys.set_var("Y", sim.Ys + 1.0)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

# Note: if you want to modify the figure and/or save it, you can use
# ax = plt.gca()
# fig = ax.figure

sim.output.print_stdout.plot_XY()
plt.savefig(path_dir_save + '/Predaprey_XY.png')
if '--no-show' not in sys.argv:
    plt.show()

sim.output.print_stdout.plot_XY_vs_time()
plt.savefig(path_dir_save + '/Predaprey_XYplot_vs_time.png')
if '--no-show' not in sys.argv:
    plt.show()