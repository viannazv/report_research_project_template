\documentclass[a4paper,12pt]{article}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts}
\usepackage{amssymb}

\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{lmodern}
\usepackage{textcomp}

\usepackage{natbib}

\usepackage[top=1.2in,bottom=1.2in,left=3.cm,right=3.cm,a4paper]{geometry}

\usepackage[font=footnotesize]{caption}

\usepackage{xcolor}

\usepackage{hyperref}

\usepackage{listings}

\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!99!black}
}

\usepackage{titling}

\pretitle{%
\includegraphics[width=0.3\linewidth]{../fig/logo_UGA}
\vspace{2cm}
\begin{center}
\LARGE
}
\posttitle{\end{center}}
\postdate{\par\end{center}\vspace{12cm}~}

\usepackage[nottoc,numbib]{tocbibind}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Practical project - Analysis of the predator-prey model and Lorenz dynamical system}
\author{Victor Emanoel VIANNA ZAIA DE ALMEIDA}

\begin{document}

\renewcommand{\labelitemi}{$\bullet$}

\maketitle

\tableofcontents

\pagebreak

\listoffigures

\newpage

\section{Introduction} \label{Introduction}

In this report, it will be presented the practical work of instabilities and turbulence. For the practical project, analyses of both the Lorenz dynamical model system and the predator-prey model were carried out.

Before presenting the results of the analyses, a bibliographic review of the theoretical concepts needed to further understand the theory behind the models will be introduced, more precisely the definition of a system with degrees of freedom, the lotka-Volterra equations and the Lorenz model. Once the theoretical basis of the analyses are stated, the results obtained will be displayed and discussed.

\pagebreak

\section{Bibliographic review} \label{Bibliographic_Review}

On this section, all the theoretical references on which this report was based will be discussed. It will be presented the basic theory behind a system with degrees of freedom and instabilities, followed by the lotka-Volterra equations and, lastly, the Lorenz model.

\subsection{Degrees of freedom} \label{Degrees_of_freedom_and_instabilities}

When studying mechanics, a degree of freedom is an independent variable that describes the movement and possible positions of a body in space. The DOF (degress of freddom) can be divided into two types of movements: translation and rotation. The Figure~\ref{fig_DOF} displays all the possibles degrees of freedom when studying the movement of a body in space.

\begin{figure}[!h]	
	\centering
	\includegraphics[width=0.7\linewidth]{../fig/DOF}
	\caption{Translations and rotations of a body in space.}
	\label{fig_DOF}
\end{figure}

\subsection{Instabilities}

Generally speaking, a unstable system can be characterize by some output or internal state that grows without bounds. A common example inside structural engineering would be bucking phenomenon on plates or columns, as shown in Figure~\ref{Beam_Buckling} and Figure~\ref{Panel_Buckling}, respectively.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.4\linewidth]{../fig/Buckling}
	\caption{Buckling of a beam under compressive load.}
	\label{Beam_Buckling}
	\vspace{1cm}
	\includegraphics[width=0.6\linewidth]{../fig/Panel_Buckling}
	\caption{Buckling of a panel using finit elements.}
	\label{Panel_Buckling}
\end{figure}

When analyzing instabilities of a system, it is of paramount importance to also know the definition of fixed points. In order to find the fixed points of a system, described by a function f(x), the derivative has to be computed and the values in which the derivative is equal to zero are the fixed points (Equation~\ref{Fixed_Points_def}).

\begin{equation}
	\label{Fixed_Points_def}
	\frac{df(x)}{dx} = 0
\end{equation}

\subsection{Lotka-Volterra equations} \label{Lotka_Volterra_equations}

\subsubsection{Definition} \label{Lotka-Volterra-equations_Definition}

The Lotka-Volterra equations, also known as predator-prey equations, are a pair of non linear differential equations of first order normally used to describe the dynamical behavior of biological system. The Equation~\ref{First_predPrey_eq} and Equation~\ref{Second_predPrey_eq} display the predator-prey relationship.

\begin{equation}
	\label{First_predPrey_eq}
	\frac{dX}{dt} = (A - BY)*X
\end{equation}

\begin{equation}
	\label{Second_predPrey_eq}
	\frac{dY}{dt} = (DX - C)*Y
\end{equation}

where:

\begin{itemize}
	\item t: is time;
	\item X: number of prey;
	\item Y: number of predators;
	\item A, B, C, D: positive real parameters.
\end{itemize}

\subsubsection{Fixed points} \label{Lotka_Volterra_equations_FixedPoints}

In this section it will be discussed how to find the fixed points of the predator-prey equations and evaluate their stability.

For computing the fixed points, in other words, the values where $(\dot X, \ \dot Y)= (0, \ 0)$, it is needed to linearize the equations using partial derivatives. At this point, the Jacobian matrix for this system will be applied.

\begin{equation}
	J = 
	\begin{bmatrix}
		\frac{df_1}{dx_1} & \frac{df_1}{dx_2} \\
		\frac{df_2}{dx_1} & \frac{df_2}{dx_2}
	\end{bmatrix}
\end{equation}

where:

\begin{itemize}
	\item \textbf{f} =
	$\begin{bmatrix}
		f_1(x, y) \\ f_2(x,y)
	\end{bmatrix}$
\end{itemize}

In the current problem, the Jacobian matrix can be expressed as:

\begin{equation}
	J(X, Y) = 
	\begin{bmatrix}
		(A - BY) & -BX \\
		DY & (DX - C)
	\end{bmatrix}
\end{equation}

The trivial solution would be when $(X, \ Y)= (0, \ 0)$, therefore the Jacobian matrix can be simplified, as shown in Equations~\ref{Jacobian_predPrey_trivial}, but this solution means that both the species have zero invidious.

\begin{equation}
	\label{Jacobian_predPrey_trivial}
	J = 
	\begin{bmatrix}
		A & 0 \\
		0 & -C
	\end{bmatrix}
\end{equation}

Other solution that can be computed, is when $(X, \ Y)= (C/D, \ A/B)$. On this conditions the Jacobian will become:

\begin{equation}
	J = 
	\begin{bmatrix}
		0 & \frac{-BC}{D} \\
		\frac{AD}{B} & 0
	\end{bmatrix}
\end{equation}

Once the Jacobian is determined, an eigenvalue and eigenvector analysis can be carried out, resulting into $\lambda = (i\sqrt{AD}, \ -i\sqrt{AD})$. As shown, both the eigenvalue are purely imaginary and conjugated to each other, hence these points have to be a center for a closed orbits, as it is being displayed in Figure~\ref{Predator_Prey_Exemple}.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\linewidth]{../fig/Pred-Prey_exemple}
	\caption{Exemple of a predator-prey model.}
	\label{Predator_Prey_Exemple}
\end{figure}

\subsection{Lorenz Model}  \label{Lorenz_Model}

\subsection{Definition}  \label{Lorenz_Model_Definition}

The lorezn model is a system of ordinary differential equations. Differently from the Lotka-Volterra equations, the Lorenz model has one additional degree of freedom, which will turn the system into a non periodic orbit and strongly sensitive to the initial conditions.

The model can be described by three ordinary differential equations known as the Lorenz equations, as presented in Equation~\ref{Lorenz_eq}.

\begin{equation}
	\label{Lorenz_eq}
	\begin{split}
		\frac{dX}{dt} = \sigma * (Y - X) \\
		\frac{dY}{dt} = \rho X - Y - XZ \\
		\frac{dZ}{dt} = XY - \beta Z
	\end{split}
\end{equation}

\subsubsection{Fixed points}   \label{Lorenz_Model_FixedPoints}

By applying the same procedure shown in item~\ref{Lotka_Volterra_equations_FixedPoints}, it is possible to find the eigenvalues for each fixed points of the system, as shown below, and analyze their stability state.

\begin{align}
(X, Y, Z) = (0, 0, 0) \\
(X, Y, Z) = (\frac{\beta}{\rho}, \frac{\beta}{\rho}, 1)
\end{align}

\pagebreak

\section{Results}

In this section the numerical simulation for both the predator-prey and Lorenz model will be presented. It is worth mentioning that the simulation were done using \texttt{fluidsim} library.

\subsection{Predator-Prey model}

The Figure~\ref{Predator_Prey_time} presents the result of the analysis, showing the population as a function of time.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.8\linewidth]{../fig/Predaprey_XYplot_vs_time}
	\caption{Evolution of population over time.}
	\label{Predator_Prey_time}
\end{figure}

As for the Figure~\ref{Predator_Prey_XY}, it is being presented the population as a function of the predators and preys. From the image, it is possible to see the fixed point of the system, which is indicated by a cross sign.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.8\linewidth]{../fig/Predaprey_XY}
	\caption{Evolution of population over time.}
	\label{Predator_Prey_XY}
\end{figure}

\subsection{Lorenz model}

The Figure~\ref{Lorenz_model} shows the 3D result of the Lorenz system. The two cross sign represents the fixed points of the system.

\begin{figure}[!h]
	\centering
	\includegraphics[width=1.0\linewidth]{../fig/Lorenz_XYZplot}
	\caption{3D representation of Lorenz model.}
	\label{Lorenz_model}
\end{figure}

\pagebreak

\section{Personal subject - Boundary layer}

In physics, a boundary layer can be defined as the thin layer of fluid located at the vicinity of a boundary surface, where the fluid flows along. The interaction of the fluid with the surface is given by a condition of no-slip, in other words, zero velocity at the contact. As the boundary layer grows,the velocity increases above the surface, until it return to the main fluid velocity.

A boundary layer can be classified into a laminar and a turbulent layer. A laminar layer is characterized by a very smooth flow, creating less friction drag on the surface, but being less stable. At some distance from the start of the surface, the  laminar flow breaks down and transitions into a turbulent flow. The Figure~\ref{Laminar_Boundary_Layer} display an example of a laminar flow, whereas Figure~\ref{Boundary_Layer} shows the evolution of a laminar into a turbulent flow.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.8\linewidth]{../fig/Laminar_Boundary_layer}
	\caption{Representation of a boundary layer.}
	\label{Laminar_Boundary_Layer}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.8\linewidth]{../fig/BoundaryLayer}
	\caption{Representation of a boundary layer.}
	\label{Boundary_Layer}
\end{figure}

\pagebreak

\bibliographystyle{jfm}
\bibliography{./biblio}

\pagebreak

\section{Annex I - Codes}

\subsection{Predator-Prey model}

\begin{lstlisting}

import os
import sys

import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.predaprey.solver import Simul

here = os.path.abspath(os.path.dirname(__file__))
path_dir_save = os.path.abspath(here + '/../fig/')
if not os.path.exists(path_dir_save):
os.mkdir(path_dir_save)

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.1
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs + 2.0)
sim.state.state_phys.set_var("Y", sim.Ys + 1.0)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

# Note: if you want to modify the figure and/or save it, you can use
# ax = plt.gca()
# fig = ax.figure

sim.output.print_stdout.plot_XY()
plt.savefig(path_dir_save + '/Predaprey_XY.png')
if '--no-show' not in sys.argv:
plt.show()

sim.output.print_stdout.plot_XY_vs_time()
plt.savefig(path_dir_save + '/Predaprey_XYplot_vs_time.png')
if '--no-show' not in sys.argv:
plt.show()

\end{lstlisting}

\subsection{Lorenz model}

\begin{lstlisting}

import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.lorenz.solver import Simul
from pathlib import Path

here = Path(__file__).absolute().parent
path_dir_save = here /"../fig/"
path_dir_save.mkdir(exist_ok=True)

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.02
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs0 + 2.0)
sim.state.state_phys.set_var("Y", sim.Ys0 - 1.0)
sim.state.state_phys.set_var("Z", sim.Zs0 - 10.0)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XYZ()
fig=plt.gcf()
fig.savefig(path_dir_save / "Lorenz_XYZplot.png")

sim.output.print_stdout.plot_XY_vs_time()
fig=plt.gcf()
fig.savefig(path_dir_save / "Lorenz_XY_vs_time.png")
# see also the other plot_* methods of sim.output.print_stdout

plt.show()

\end{lstlisting}

\end{document}
